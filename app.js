'use strict';

const http = require('http');
const ejs = require('ejs');
const axios = require('axios').default;
const redisClient = require('redis').createClient();
redisClient.connect();

http.createServer(async (req, res) => {
    let filePath = req.url;
    if (filePath != '/index.ejs') return res.end('404')
    let photo = await redisClient.get('photo')
    if (photo === null) {
         let result = await axios.get('https://api.nasa.gov/planetary/apod', {
            params: {
                api_key: 'DEMO_KEY',
                date: '2020-01-02'
            }
        })
        redisClient.set('photo', JSON.stringify(result.data));
        photo = JSON.stringify(result.data)
    }

    ejs.renderFile(`public${filePath}`, { photo: photo }, function (err, str) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(str, 'utf-8');
    })
}).listen(3000)